'use strict';
// Declare app level module which depends on views, and components
angular.module('project.controller',['ngResource','ngRoute'])






.controller('AddProjectCtrl', function($scope,showCategoriesFactory,AddProjectFactory) {

	
	//AddProjectFactory
	//showCategoriesFactory
    $scope.project=new AddProjectFactory();
	$scope.categories= new showCategoriesFactory.query();
	$scope.addProject=function(){
	$scope.projectImage="http://demo.goodlayers.com/greennature/wp-content/uploads/2013/12/shutterstock_133689230-700x400.jpg";
    $scope.project.$save();

  }
	
		//$scope.addProject = function(projectName,projectDescription,projectImage) {
            //    console.log(projectImage);

				//projectFactory.addP(projectName,projectDescription,projectImage,function(res) {
                   
		//		})

			//}
		
	

})


.controller('CommentManageCtrl', ['$scope', function ($scope) {
 $scope.comments = [];
 $scope.addComment = function(cmt){
  cmt.avatar = md5(cmt.email.trim().toLowerCase());
  $scope.comments.push(angular.copy(cmt));
 };
}])





.controller('ProjectByCategCtrl', function($scope,$routeParams,$resource) {
  $scope.projects= $resource('https://bledi--ndhifa.herokuapp.com/projects/category/'+$routeParams.id).query();
     
	return $scope.projects;
      })


.controller('OtherProjectsCtrl', function($scope,$routeParams,$resource) {
   $scope.projects= $resource('https://bledi--ndhifa.herokuapp.com/projects/otherProjects/'+$routeParams.id).query();
     
	return $scope.projects;
      })












.controller('ShowProjectCtrl', function($scope,showProjectFactory) {
  $scope.projects=showProjectFactory.query();
return $scope.projects;
	
})
.controller('ShowProjectDetailsCtrl',function($rootScope,$scope,$routeParams,$sessionStorage,projectFactory){ 
			$scope.project =   projectFactory.showdet($routeParams.id,function(res) {
				$scope.project = res;
				 //  $scope.project = res;

				
				$scope.idn=$routeParams.id;
				console.log($routeParams.id);
				
          return($scope.project);

			})

		   
		   
		   
})



.controller('donateProjectCtrl',function($rootScope,$log,$scope,$routeParams,$sessionStorage,projectFactory){ 
			$scope.project =   projectFactory.showdet($routeParams.id,function(res) {
				$scope.project = res;
				   $sessionStorage.project = res;

          return($sessionStorage.project);

			})

		   
$scope.idn=$routeParams.id;
				console.log($routeParams.id);		   
		   
})

.controller('sponsorProjectCtrl',function($rootScope,$log,$scope,$routeParams,$sessionStorage,sponsorFactory,findByIdFactory){ 
	
	$scope.sponsor=new sponsorFactory();
	$scope.project=findByIdFactory.query();
	$scope.sponsoriser=function(){
    $scope.sponsor.$save();
	   
}
})


 .factory('AddProjectFactory', function($resource){
    
        //Resources
       var managingResource=$resource('https://bledi--ndhifa.herokuapp.com/projects/');
          return managingResource;
    })


 .factory('showProjectFactory', function($resource){
    
        //Resources
	var managingResource=$resource('https://bledi--ndhifa.herokuapp.com/projects/');
          return managingResource;
    })



 .factory('showCategoriesFactory', function($resource){
    
	
        //Resources
	var managingResource=$resource('https://bledi--ndhifa.herokuapp.com/categories/');
          return managingResource;
    })



.factory('sponsorFactory', function($resource){
    
        //Resources
	var managingResource=$resource('https://bledi--ndhifa.herokuapp.com/sponsors');
          return managingResource;
    })


.factory('findByIdFactory', function($resource,$routeParams){
    
        //Resources
	var managingResource=$resource('https://bledi--ndhifa.herokuapp.com/projects/find/'+$routeParams.id);
          return managingResource;
    })





/* .factory('showProjectDetails', function(id,callback){
    
	var factory = {};

factory.showdet = function(id,callback) {
$http.get('http://localhost:3000/projects/find/'+id).success(function(res) {
    $sessionStorage.project = res;
callback(res);
})
}


return factory;

})*/




.directive('paypalButton', function($routeParams) {
    return {
      restrict: 'E',
      scope: {},
      compile: function(element, attrs) {
        var languageCodes = [ // PayPal allowed language codes
          'en_US',
          'es_ES',
          'fr_FR',
          'it_IT',
          'de_DE'
        ];
        var currencyCodes = [ // PayPal allowed currency codes
          'AUD',
          'CAD',
          'CZK',
          'DKK',
          'EUR',
          'HKD',
          'HUF',
          'ILS',
          'JPY',
          'MXN',
          'NOK',
          'NZD',
          'PHP',
          'PLN',
          'GBP',
          'RUB',
          'SGD',
          'SEK',
          'CHF',
          'TWD',
          'THB',
          'USD'
        ];
        var buttonSizes = [ // PayPal allowed button sizes
          'SM', // small
          'LG' // large
        ];
        var name = this.name;
        function err(reason) {
          element.replaceWith('<span style="background-color:red; color:black; padding:.5em;">' + name + ': ' + reason + '</span>');
          console.log(element.context);
        }
        var action = attrs.action || 'https://www.paypal.com/us/cgi-bin/webscr';
        var business = attrs.business;
        var languageCode = attrs.languageCode || 'en_US';
        var currencyCode = attrs.currencyCode || 'USD';
        var itemName = attrs.itemName;
        var amount = parseFloat($routeParams.price);
		
        var buttonSize = attrs.buttonSize || 'SM';
        var imgAlt = attrs.imgAlt || 'Make payments with PayPal - it\'s fast, free and secure!';
        if (!business) { return err('business not specified!'); }
        if (!itemName) { return err('item name not specified!'); }
        if (!amount) { return err('amount not specified!'); }
        if (isNaN(amount)) { return err('amount is not a number!'); }
        if (languageCodes.indexOf(languageCode) < 0) { return err('unforeseen language code!'); }
        if (currencyCodes.indexOf(currencyCode) < 0) { return err('unforeseen currency code!'); }
        if (buttonSizes.indexOf(buttonSize) < 0) { return err('unforeseen button size!'); }
        var imgSrc = 'http://www.paypalobjects.com/' + languageCode + '/i/btn/btn_buynow_' + buttonSize + '.gif';
        var template =
          '<form name="_xclick" action="' + action + '" method="post">' +
          '<input type="hidden" name="cmd" value="_xclick">' +
          '<input type="hidden" name="business" value="' + business + '">' +
          '<input type="hidden" name="currency_code" value="' + currencyCode + '">' +
          '<input type="hidden" name="item_name" value="' + itemName + '">' +
          '<input type="hidden" name="amount" value="' + amount + '">' +
          '<input type="image" src="' + imgSrc + '" border="0" name="submit" alt="' + imgAlt + '">' +
          '</form>';
        //element.replaceWith(template);
        element.append(template);
      }
    };
  })




	