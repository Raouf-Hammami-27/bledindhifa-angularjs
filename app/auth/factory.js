
    angular.module("myApp.auth.factory",[])
        .factory("UserService", function($http) {
        var api = {
            login: login,
            logout: logout,
            register: register,
            findAllUsers: findAllUsers,
            deleteUser: deleteUser,
            updateUser: updateUser,
            createUser: createUser
        };
        return api;

        function logout() {
            return $http.post("http://bledi--ndhifa.herokuapp.com/api/logout");
        }

        function createUser(user) {
            return $http.post('http://bledi--ndhifa.herokuapp.com/api/user', user);
        }

        function updateUser(userId, user) {
            return $http.put('http://bledi--ndhifa.herokuapp.com/api/user/'+userId, user);
        }

        function deleteUser(userId) {
            return $http.delete('http://bledi--ndhifa.herokuapp.com/api/user/'+userId);
        }

        function findAllUsers() {
            return $http.get("http://bledi--ndhifa.herokuapp.com/api/user");
        }

        function register(user, image_id) {
            return $http.post("http://bledi--ndhifa.herokuapp.com/auth/register", user, image_id);
        }

        function login(user) {
            return $http.post("http://bledi--ndhifa.herokuapp.com/auth/signin", user);
        }
        return factory;
    })
