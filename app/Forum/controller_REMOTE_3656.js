'use strict';
// Declare app level module which depends on views, and components
angular.module('myApp.forum.controller',[])

.controller('userController',function($rootScope,$log,$scope,userFactory,$location,$routeParams,$sessionStorage,$route) {
	
			$scope.addUser = function() {	
				
				userFactory.addUser($scope.newUser,function(user) {

					$scope.$user = user;
					$location.path('/stat');
                var r = confirm("Successful Message!");
                    if (r == true){
                      window.location.reload();
                    }
				});
			}




			$scope.logout = function() {
				userFactory.logout();
				$location.path('/');

			}

				


			$scope.showUser = userFactory.showUser($routeParams.id,function(res) {
				$scope.showUser = res;

			})



		
		})

.controller('dashboardController',function($scope,userFactory,categoryFactory,$location,topicFactory,socket,$sessionStorage) {

			$scope.user = userFactory.user();


			$scope.$storage = $sessionStorage;

			
			$scope.categories = categoryFactory.getCategories(function(categories) {
				$scope.categories = categories;
			})

			$scope.topics = topicFactory.getTopics(function(topics) {
				$scope.topics = topics;
			})
            
            
            
           

			
			$scope.addTopic = function() {
				$scope.newTopic.userId = $scope.user._id;
				topicFactory.addTopic($scope.newTopic,function(res) {
					socket.emit('newTopic',res);
				})
				$scope.newTopic = {};
			}

			socket.on('updateTopic',function(data) {
				$scope.$apply(function() {
					$scope.topics.push(data);
				})
			})
			socket.on('updatePost',function(data) {
			
				for (var i = 0; i < $scope.topics.length; i++) {
					if ($scope.topics[i]._id == data.postId) {
						$scope.$apply(function() {
							$scope.topics[i].post.push(data.post);
						})
					}
				}


			})
		})

.controller('topicController',function($rootScope,$scope,topicFactory,$routeParams,$log,userFactory,socket,$location,$sessionStorage) {
			$scope.user = userFactory.user;

    $scope.delete = function(index){
            console.log(index);
             var topicId = index;
             topicFactory.deleteTopic(topicId,function() {
           alert("Topic removed");
        $location.path('/dashboard');

                })
            }

			$scope.topic = topicFactory.showTopic($routeParams.index,function(res) {
				$scope.topic = res;
			})


			$scope.addPost = function() {
				topicFactory.addPost($scope.newPost,function(res) {

					$scope.topic.post.push(res);
					socket.emit('newPost',{postId: $scope.topic._id,post:res});
				})
				$scope.newPost = {};
			}

			$scope.upVote = function(index) {
				var postId = $scope.topic.post[index]._id;

				topicFactory.updatePost(postId,"upVote",function(res) {

					$scope.topic.post[index].upvote = res.upvote;
				})
	
			}


			$scope.downVote = function(index) {
				var postId = $scope.topic.post[index]._id;

				topicFactory.updatePost(postId,"downVote",function(res) {

					$scope.topic.post[index].downvote = res.downvote;
				})

			}


			$scope.addComment = function(index) {

				topicFactory.addComment($scope.topic.post[index],function(res) {
	
					$scope.topic.post[index].comments.push(res);

				})
				$scope.topic.post[index].newComment = "";

			}

			$scope.logout = function() {
				userFactory.logout();
				$location.path('/');
			}
            $scope.user=$sessionStorage.user.name;
		})

.controller('SocialCtrl', function($scope,$rootScope,topicFactory,$routeParams,$sessionStorage) {
     topicFactory.showTopic($routeParams.index,function(res){
    
        $scope.title = res.text;
        $scope.description = res.description;
        $scope.data_url = "https://bledi-ndhifa.herokuapp.com/app/index.html#/topic/"+$routeParams.index;
       // $scope.data_image = p.product_Image;
        console.log($scope.title);
        console.log($scope.description);
        console.log($scope.data_url);
    return res;
    });            
 })
.controller('SocialEventCtrl', function($scope,eventsFactory,$routeParams) {
     eventsFactory.showdet($routeParams.id,function(res){
    
        $scope.current_title = res.nameEvent;
        $scope.current_description = res.description, res.startDate , res.endDate;
        $scope.data_url = "https://bledi-ndhifa.herokuapp.com/app/index.html#/events/"+$routeParams.id;
       // $scope.data_image = p.product_Image;
    return res;
    });
    
            
        })
.controller('SocialConcourCtrl', function($scope,imagesFactory,$routeParams,$sessionStorage) {
     imagesFactory.showdet($routeParams.id,function(res){
        $scope.current_title = $sessionStorage.competition.name;
        $scope.current_description =$sessionStorage.competition.description,$sessionStorage.competition.start_date,$sessionStorage.competition.end_date ;
        $scope.data_url = "https://bledi-ndhifa.herokuapp.com/app/index.html#/image/"+$routeParams.id;
        $scope.data_image = res;
    return res;
    });
    
            
        })