'use strict';

angular.module('myApp.blog.factory',[])

/*.factory('socket', ['$rootScope', function($rootScope) {
    
 var socket = io.connect();
 return {
on: function(eventName, callback){
 socket.on(eventName, callback);
			    },
 emit: function(eventName, data) {
socket.emit(eventName, data);
}
};
}])*/


.factory('blogfactory',function($http,$sessionStorage) {
var factory = {};
var blogs = [];
var currentblogId = "";	
var userId = $sessionStorage.user._id;

    

factory.getblog = function(callback) {
    
$http.get('http://bledi--ndhifa.herokuapp.com/blogs').success(function(res) {
blogs = res;
$sessionStorage.blogs = res;
callback(res);
})
}

factory.showblog = function(index,callback) {
    console.log(index);
var id = $sessionStorage.blogs[index]._id;
$sessionStorage.currentblogId = id;
    console.log(id);

$http.get('http://bledi--ndhifa.herokuapp.com/blogs/'+id).success(function(res) {
$sessionStorage.blog = res;
callback(res);
})
}

factory.addblog = function(title,Description,image_id,callback) {
var userId = $sessionStorage.user._id;
    console.log(userId);
$http.post('http://bledi--ndhifa.herokuapp.com/blogs/'+userId,{title:title,Description:Description,image_id:image_id}).success(function(res) {
callback(res);
})
}

factory.updateblog = function(callback) {
  var currentblogId = $sessionStorage.currentblogId;
    $http.patch('http://bledi--ndhifa.herokuapp.com/blogs/'+currentblogId).success(function(res) {
callback(res);

}).failures(function(res){
        console.log("testin");
    })
}

factory.addComment = function(blog,comment,callback) {
var userId = $sessionStorage.user._id;
var blogId = blog;
var comment= comment;
$http.post('http://bledi--ndhifa.herokuapp.com/blogs/comment/'+blogId,{user:userId,comment:comment}).success(function(res) {				
callback(res);
})
}
return factory;
})