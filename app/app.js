'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.forum.controller',
    'myApp.forum.factory',
    'myApp.blog.controller',
    'myApp.blog.factory',
    'myApp.stat.controller',
    'myApp.stat.factory',
	'myApp.events.controller',
    'myApp.events.factory',
    'project.factory',
    'project.controller',
    'myApp.competitions.controller',
    'myApp.concours.factory',
    'myApp.photo.controller',
    'myApp.photo.factory',
    'myApp.register.controller',
    'myApp.login.controller',
    'myApp.auth.factory',
    'pascalprecht.translate',
    'ui.bootstrap.datetimepicker',
    'ui.calendar',
    'ngStorage',
    'ngCookies',
    'angular-loading-bar',
    'ngAnimate',
    '720kb.socialshare',
    'Alertify',
    'reCAPTCHA'

        
    ])









  .config(['$routeProvider', function($routeProvider) {
        $routeProvider 
		
		
		.when('/contact',{
templateUrl:'contact.html'
			})
        .when('/login', {
                  templateUrl: 'auth/login.html',
                  controller: 'LoginCtrl',
                  controllerAs: 'model'
              })
              .when('/register/base', {
                  templateUrl: 'auth/register.html',
                  controller: 'RegisterCtrl',
                  controllerAs: 'model'
              })
            .when('/register/association', {
                  templateUrl: 'auth/registerAssociation.html',
                  controller: 'RegisterAssociationCtrl',
                  controllerAs: 'model'
              })
       .when('/register',{
templateUrl:'auth/registerhome.html'
			})
            .when('/profile',{
templateUrl:'auth/profile.html'
			})
            .when('/donate',{
templateUrl:'association/association.html'
			})
		.when('/blog',{
templateUrl:'blog/blog3.html'
			})
            .when('/blog/rss',{
templateUrl:'blog/rssPost.html'
			})
            .when('/blog/new',{
templateUrl:'blog/addBlog.html'
			})
            .when('/blog/:index',{
templateUrl:'blog/post.html'
			})
          
           
			.when('/dashboard', {
templateUrl:'Forum/Forum.html'
			})
			.when('/topic/:index', {
templateUrl:'Forum/topic.html'		
			})
			.when('/users/:id', {
templateUrl:'Forum/profile.html'})
            .when('/', {
templateUrl:'stat/stats.html'}) 
            .when('/stat/tunisie', {
templateUrl:'stat/statTunisie.html'}) 

            .when('/events', {
templateUrl:'events/events.html'}) 
        
            .when('/events/:id', {
templateUrl:'events/info.html'       
        })
            .when('/participate/:id', {
templateUrl:'events/info.html'       
        })
        
        	.when('/event/add',{
            templateUrl:'events/add.html'
           
            })
        
		
		//rim
        
			.when('/ajout',{
		templateUrl:'Project/create.html',
			controller:'AddProjectCtrl'
           
            })
		
		.when('/projects',{
                templateUrl:'Project/blog-masonry.html',
						controller:'ProjectByCategCtrl'

           
            })
		
		.when('/donate/:id/:price',{
		
		  templateUrl:'Project/donate.html',
			controller:'donateProjectCtrl'
		})
		
		
			.when('/sponsor/:id',{
		
		  templateUrl:'Project/sponsor.html',
			controller:'sponsorProjectCtrl'
		})
		
		
		 .when('/projects/:id',{
		
		  templateUrl:'Project/blog-masonry.html',
		  controller:'ProjectByCategCtrl'
		})
		
		
		
		.when('/project/:id/:long/:lat/:price',{
                templateUrl:'Project/open-project.html',
			 controller:'ShowProjectDetailsCtrl'

           
            })
		
		.when('/admin',{
                templateUrl:'admin/datatables.html'
		

           
            })
		
		
		
		.when('/quiz',{
		templateUrl:'quizapp-angular-master/index.html'
		
		})
		
		.when('/test',{
		templateUrl:'quizapp-angular-master/index.html'
		
		})
		
		.when('/suivant',{
		templateUrl:'Project/create1.html',
		controller:'suivantCtrl'
		})
		
			.when('/plus',{
		templateUrl:'Project/create2.html',
		controller:'suivantCtrl'
		
		})
		


		
		
		
		
		//fin rim
        
        .when('/concours', {
templateUrl:'concours/concours.html'}) 
		
		 .when('/concours/:id', {
templateUrl:'concours/info.html'  
        })
        
        	.when('/upload/:id', {
templateUrl:'concours/upload.html'  
        })
        
        .when('/image/:id', {
templateUrl:'photo/info.html'  
        })
            
        .when('/erreur', {
templateUrl:'404.html'  
        })
        
        
        
        .when('/calender', {
			  	templateUrl: 'calender/calendar.html',
			  	controller: 'CalenderController',
			  	controllerAs: 'calenderVm',
			  	resolve: {
			  		events: ['dataService', function(dataService) {
			  			return dataService.getEvents();
			  		}]
			  	}
		  	})
        
        
        
        
        
    .otherwise({redirectTo: '/erreur'});
    }])
    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
      }])
    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.latencyThreshold = 500;
      }])



.config(['$translateProvider', function ($translateProvider) {
       
    $translateProvider.registerAvailableLanguageKeys(['en', 'fr'], {
            'en_*': 'en',
            'en-*': 'en',
            'fr_*': 'fr',
            'fr-*': 'fr',
            '*': 'fr'
        });

        $translateProvider.useStaticFilesLoader({
            prefix: 'locale/locale-',
            suffix: '.json'
        });
    
    
    
    
   /*$translateProvider.useLocalStorage();*/

        // register a fallback language (i. e. if an ID isn't available
        // in the chosen language, then use the fallback language)
        $translateProvider.fallbackLanguage('fr');

        // set default language
        // $translateProvider.preferredLanguage('en');
        // try to find out preferred language automatically
        $translateProvider.determinePreferredLanguage('fr');
 
    
    $translateProvider.useLocalStorage();
    

      
    }])


 .controller('TranslateCtrl', ['$scope', '$translate', function ($scope, $translate) {
   
    $scope.changeLanguage = function (key) {
    $translate.use(key);
        
  };
}])
.directive('quiz', function(quizFactory) {
	
	return {
		restrict: 'AE',
		scope: {},
		templateUrl: 'template.html',
		link: function(scope, elem, attrs) {
			scope.start = function() {
				scope.id = 0;
				scope.quizOver = false;
				scope.inProgress = true;
				scope.getQuestion();
			};

			scope.reset = function() {
				scope.inProgress = false;
				scope.score = 0;
			}

			scope.getQuestion = function() {
				var q = quizFactory.getQuestion(scope.id);
				if(q) {
					scope.question = q.question;
					scope.options = q.options;
					scope.answer = q.answer;
					scope.answerMode = true;
				} else {
					scope.quizOver = true;
				}
			};

			scope.checkAnswer = function() {
				if(!$('input[name=answer]:checked').length) return;

				var ans = $('input[name=answer]:checked').val();

				if(ans == scope.options[scope.answer]) {
					scope.score++;
					scope.correctAns = true;
				} else {
					scope.correctAns = false;
				}

				scope.answerMode = false;
			};

			scope.nextQuestion = function() {
				scope.id++;
				scope.getQuestion();
			}

			scope.reset();
		}
	}
})

.factory('quizFactory', function() {
	var questions = [
		{
			question: " Qu’est-ce que le compost ?",
			options: ["Un fruit", "Un phénomène naturel de décomposition des végétaux","Une filiale de La Poste"],
			answer: 1
		},
		{
			question: "Dans quels déchets trouve-t-on des produits polluants ou des métaux lourds ?",
			options: ["Les briques de jus de fruits", "Les boîtes de conserves", "Les batteries et piles"],
			answer: 2
		},
		{
			question: "Combien de temps prend une bouteille en verre pour se dégrader ?",
			options: ["50 ans", "100 ans", "500 ans", "4 000 ans"],
			answer: 3
		},
		{
			question: "Laquelle de ces trois substances que l'on trouve dans l'eau du robinet n'est pas nocive pour la santé ?",
			options: ["Le chlore", "Le nitrate", "Le plomb"],
			answer: 0
		},
		{	
			question: "Quels sont les sacs les moins polluants ?",
			options: ["Les sacs plastiques jetables", "Les sacs plastiques réutilisables", "Les sacs en papier"],
			answer: 1
		}
	];

	return {
		getQuestion: function(id) {
			if(id < questions.length) {
				return questions[id];
			} else {
				return false;
			}
		}
	};
})



.config(function (reCAPTCHAProvider) {

                       // required, please use your own key :)
                       reCAPTCHAProvider.setPublicKey('6LfyK-0SAAAAAAl6V9jBGQgPxemtrpIZ-SPDPd-n');

                       // optional
                       reCAPTCHAProvider.setOptions({
                           theme: 'clean'
                       });
                   })


.directive('locationPredictions', [
  function() {
    return {
      restrict: 'E',
      scope: { results: '=' },
      template: '<input type="text" placeholder="search for a location">',
      link: function(scope, iElement, iAttrs) {

        // Setup Google Auto-complete Service
        var googleMapsService = new google.maps.places.AutocompleteService();
        var el = angular.element(iElement.find('input'));

        // Fetch predictions based on query
        var fetch = function(query) {
          googleMapsService.getPlacePredictions({
            input: query
          }, fetchCallback);
        };

        // Display predictions to the user
        var fetchCallback = function(predictions, status) {

          if (status !== google.maps.places.PlacesServiceStatus.OK) {

            scope.$apply(function() {
              scope.results = [];
            })

            return;

          } else {

            scope.$apply(function() {
              scope.results = predictions;
            })
          }
        };


        // Refresh on every edit
        el.on('input', function() {
          var query = el.val();

          if (query && query.length >= 3) {

            fetch(query);

          } else {

            scope.$apply(function() {
              scope.results = [];
            });
          }
        });

      }
    }
  }
])
.directive('notification', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            template:"<div class='alert alert-{{alertData.type}}' ng-show='alertData.message' role='alert' data-notification='{{alertData.status}}'>{{alertData.message}}</div>",
            scope:{
              alertData:"="
            }
        };
    }])




